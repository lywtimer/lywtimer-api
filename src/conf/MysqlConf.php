<?php

namespace mszl\api\conf;

use mszl\api\utils\Entity;
use mszl\api\utils\IniReader;

/**
 * $this->dbname, $this->server, $this->user, $this->password
 * @property mixed $database
 * @property mixed $host
 * @property mixed $port
 * @property mixed $user
 * @property mixed $password
 */
class MysqlConf extends Entity
{

    public static function get(array $conf): MysqlConf
    {
        $default = [
            'host' => 'localhost',
            'port' => 3306,
            'user' => 'root',
            'password' => 'root',
            'database' => 'mysql',
        ];
        return new self($conf + $default);
    }

}