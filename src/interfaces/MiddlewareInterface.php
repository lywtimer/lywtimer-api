<?php

namespace mszl\api\interfaces;


use mszl\api\middlewares\MiddlewareStack;

interface MiddlewareInterface
{
    public function handle(Context $context, MiddlewareStack $stack);

    public function onException($e);
}