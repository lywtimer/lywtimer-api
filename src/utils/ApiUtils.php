<?php

namespace mszl\api\utils;
use Exception;
use mszl\api\traits\BaseUtils;
use Symfony\Component\HttpFoundation\Request;
use Predis\Client;

class ApiUtils
{
    use BaseUtils;
    const API_CODE_SUCCESS = 1;
    const API_CODE_SERVICE_ERROR = 500;
    const API_CODE_LOGIN_ERROR = 401;

    public static ?Client $redisClient = null;

    public static function getRedis(): Client
    {
        if (!self::$redisClient) {
            $client = new Client(env('REDIS_DNS'));
            $client->auth(env('REDIS_PASSWORD'));
            self::$redisClient = $client;
        }
        return self::$redisClient;
    }

    public static array $storage = [];
    public static function setUserinfo($userinfo): void
    {
        self::$storage['userinfo'] = $userinfo;
    }

    public static function getUserinfo()
    {
        return self::$storage['userinfo'] ?? [];
    }

    public static function setToken($token): void
    {
        self::$storage['token'] = $token;
    }

    public static function getToken(): string
    {
        return self::$storage['token'] ?? '';
    }
    public static function setQueryParams($params): void
    {
        self::$storage['query'] = $params;
    }

    public static function getQueryParams($field = null, $default = null)
    {
        $params = self::$storage['query'] ?? [];
        if (is_null($field)) {
            return $params;
        }

        return $params[$field] ?? $default;
    }

    public static function setParams($params): void
    {
        self::$storage['params'] = $params;
    }

    public static function getParams($field = null, $default = null)
    {
        $params = self::$storage['params'] ?? [];
        if (is_null($field)) {
            return $params;
        }

        return $params[$field] ?? $default;
    }

    public static function success($data, $msg = '', $code = self::API_CODE_SUCCESS): array
    {
        return compact('code', 'msg', 'data');
    }

    public static function error($msg = '', $code = self::API_CODE_SERVICE_ERROR, $data = null): array
    {
        return compact('code', 'msg', 'data');
    }

    public static function input(Request $request, $field = null, $default = null)
    {
        $requestParams = json_decode($request->getContent(), true);

        if (is_null($field)) {
            return $requestParams;
        }

        return $requestParams[$field] ?? $default;
    }

    /**
     * @throws Exception
     */
    public static function upload(Request $request, $path = "images"): array
    {
        if ($file = $request->file('photo')) {
            $size = $file->getSize(); //412887
            //大小控制，图片裁剪
            if ($size > 2000 * 1024) {
                throw new Exception('Exceeded file size limit');
            }

            $path = $file->store("public/$path");
            return [
                'pic_id' => str_replace("public/", "", $path),
                'avatar' => self::pic(str_replace("public/", "", $path)),
            ];
        }
        throw new Exception('file not found');
    }

    public static function password(string $str): string
    {
        return md5($str . env("PASSWORD_SALT"));
    }

    public static function passwordValidate(string $check, string $passwd): bool
    {
        return md5($check . env("PASSWORD_SALT")) === $passwd;
    }

    public static function ImageAudit() {

    }
}
