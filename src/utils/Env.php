<?php

namespace mszl\api\utils;

class Env
{
    protected static ?array $repository = null;

    public static function get($key, $default = null)
    {
        if (static::$repository === null) {
            $dotenv = \Dotenv\Dotenv::createImmutable(["../"]);
            static::$repository = $dotenv->load();
        }
        return static::$repository[$key] ?? $default;
    }
}