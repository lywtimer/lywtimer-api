<?php

namespace mszl\api\traits;


trait BaseUtils
{
    public static function now(): string
    {
        return date("Y-m-d H:i:s");
    }

    public static function pic($id): string
    {
        return asset("storage/$id");
    }


}
